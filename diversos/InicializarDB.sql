﻿DELETE FROM auth_user_profile WHERE user_id > 1;
SELECT setval('auth_user_profile_id_seq', COALESCE((SELECT MAX(id) FROM auth_user_profile),1));

DELETE FROM auth_user WHERE id > 1;
SELECT setval('auth_user_id_seq', COALESCE((SELECT MAX(id) FROM auth_user),1));

DELETE FROM avaliacao;
SELECT setval('avaliacao_id_seq', COALESCE((SELECT MAX(id) FROM avaliacao),1));
