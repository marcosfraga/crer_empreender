-- noinspection SqlNoDataSourceInspectionForFile
-- noinspection SqlDialectInspectionForFile

select
  'INSERT INTO categoria (id,nome,icone) VALUES(' ||
  id || ',' ||
  '''' || nome || ''',' ||
  '''' || icone || ''');'
from categoria
order by id;

INSERT INTO categoria (id,nome,icone) VALUES(1,'Aulas','fa fa-graduation-cap');
INSERT INTO categoria (id,nome,icone) VALUES(2,'Design e Tecnologia','fa fa-laptop');
INSERT INTO categoria (id,nome,icone) VALUES(3,'Conserto Veículos','fa fa-car');
INSERT INTO categoria (id,nome,icone) VALUES(4,'Serviços Dométicos','el el-broom');
INSERT INTO categoria (id,nome,icone) VALUES(5,'Assistência Técnica','el el-wrench');
INSERT INTO categoria (id,nome,icone) VALUES(6,'Eventos e Festas','el el-glass');
INSERT INTO categoria (id,nome,icone) VALUES(7,'Moda e Beleza','fa fa-female');
INSERT INTO categoria (id,nome,icone) VALUES(8,'Consultoria','fa fa-briefcase');
INSERT INTO categoria (id,nome,icone) VALUES(9,'Reformas','el el-home');
INSERT INTO categoria (id,nome,icone) VALUES(10,'Saúde','fa fa-heartbeat');

SELECT setval('categoria_id_seq', (SELECT MAX(id) FROM categoria));