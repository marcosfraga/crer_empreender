# -*- coding: utf-8 -*-
from django import forms

from avaliacao.models import Avaliacao
from django.contrib.auth.models import User


class AvaliacaoForm(forms.ModelForm):
    profissional = forms.ModelChoiceField(
        queryset=User.objects.all().order_by('first_name'),
        empty_label="Escolha um profissional",
        label='Profissional',
        widget=forms.Select(attrs={'class': 'select'})
    )

    class Meta:
        model = Avaliacao

        exclude = [
            'id',
        ]
