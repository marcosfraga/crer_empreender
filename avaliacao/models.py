# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User


class Avaliacao(models.Model):

    profissional = models.ForeignKey(
        User,
        db_column='profissional_id',
        related_name='aval_profissional_id_fk',
    )
    avaliador = models.ForeignKey(
        User,
        db_column='avaliador_id',
        related_name='aval_avaliador_id_fk',
    )
    nota_atendimento = models.IntegerField(
        u'Atendimento',
    )
    nota_pontualidade = models.IntegerField(
        u'Pontualizade',
    )
    nota_preco = models.IntegerField(
        u'Preço',
    )
    nota_qualidade = models.IntegerField(
        u'Qualidade',
    )
    nota_recomendacao = models.IntegerField(
        u'Recomendação',
    )
    data = models.DateField(
        u'Data',
    )
    hora = models.TimeField(
        u'Hora',
    )

    class Meta:
        db_table = 'avaliacao'

    def __unicode__(self):
        return self.profissional.nome
