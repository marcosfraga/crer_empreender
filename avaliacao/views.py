# -*- coding: utf-8 -*-
import json

from datetime import datetime

from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from rest_framework.decorators import api_view
from django.http import HttpResponse

from avaliacao.forms import AvaliacaoForm
from avaliacao.models import Avaliacao


# -= NOVO =-
@login_required
def novo(request):

    if request.POST:
        form = AvaliacaoForm(request.POST)
        if form.is_valid():
            form.save()
            return render(request, 'avaliacao/avaliacao_efetuada.html', {})
        else:
            print "--> ERROR: " + str(form.errors)
    else:
        form = AvaliacaoForm()
        form.fields['avaliador'].initial = request.user.id
        form.fields['data'].initial = datetime.now().strftime("%Y-%m-%d")
        form.fields['hora'].initial = datetime.now().strftime("%H:%M")

    return render(request, 'avaliacao/form.html', {'form': form})


@api_view(['POST'])
def api_avaliacao(request):

    if request.POST:

        try:
            avaliacao = Avaliacao()

            avaliacao.profissional_id = request.POST['prof_id']
            avaliacao.nota_atendimento = request.POST['aval_atend']
            avaliacao.nota_qualidade = request.POST['aval_qualid']
            avaliacao.nota_pontualidade = request.POST['aval_pontual']
            avaliacao.nota_preco = request.POST['aval_prec']
            avaliacao.nota_recomendacao = request.POST['aval_recom']
            avaliacao.avaliador_id = request.POST['user_id']
            avaliacao.data = datetime.now().strftime("%Y-%m-%d")
            avaliacao.hora = datetime.now().strftime("%H:%M")

            avaliacao.save()

            ret_json = {
                'mensagem': 'Avaliação salva!',
                'status': 'OK'
            }
        except Exception as e:
            ret_json = [{
                'mensagem': e.message,
                'status': 'ERRO'
            }]

        return HttpResponse(json.dumps(ret_json), "application/json")
