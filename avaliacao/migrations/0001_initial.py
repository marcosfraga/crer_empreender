# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Avaliacao',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nota_atendimento', models.IntegerField(verbose_name='Atendimento')),
                ('nota_pontualidade', models.IntegerField(verbose_name='Pontualizade')),
                ('nota_preco', models.IntegerField(verbose_name='Pre\xe7o')),
                ('nota_qualidade', models.IntegerField(verbose_name='Qualidade')),
                ('nota_recomendacao', models.IntegerField(verbose_name='Recomenda\xe7\xe3o')),
                ('data', models.DateField(verbose_name='Data')),
                ('hora', models.TimeField(verbose_name='Data')),
                ('avaliador', models.ForeignKey(related_name='aval_avaliador_id_fk', db_column=b'avaliador_id', to=settings.AUTH_USER_MODEL)),
                ('profissional', models.ForeignKey(related_name='aval_profissional_id_fk', db_column=b'profissional_id', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'avaliacao',
            },
        ),
    ]
