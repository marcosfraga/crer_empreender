# -*- coding: utf-8 -*-
import json

from django.db import connection
from django.shortcuts import render
from rest_framework.decorators import api_view
from django.http import HttpResponse

from categoria.models import Categoria
from tools.views import dict_fetch_all


def index(request):
    form_caption = "Pesquisar Profissional"

    return render(request, 'pesquisa/index.html', {'form_caption': form_caption})


def categoria(request,id):
    if int(id) > 0:
        sql_script = "SELECT"
        sql_script += " u.first_name,"
        sql_script += " u.last_name,"
        sql_script += " u.email,"
        sql_script += "COALESCE(CAST((SELECT AVG((nota_atendimento+nota_pontualidade+nota_preco+nota_qualidade+nota_recomendacao)/5) " \
                                    "from avaliacao AS a " \
                                    "WHERE a.profissional_id=u.id) as integer),0) AS avaliacao, "
        sql_script += " up.fone,"
        sql_script += " up.servico,"
        sql_script += " c.nome AS cat_nome "
        sql_script += "FROM auth_user AS u "
        sql_script += "LEFT JOIN auth_user_profile AS up ON u.id = up.user_id "
        sql_script += "LEFT JOIN categoria AS c ON up.categoria_id = c.id "
        sql_script += "WHERE up.categoria_id = " + id + " "
        sql_script += "AND u.is_active=True "
        sql_script += "AND up.tipo = 'PRO' "
        sql_script += "ORDER BY avaliacao DESC, u.first_name, u.last_name"

        # print "--> sql: " + sql_script

        cursor = connection.cursor()
        cursor.execute(sql_script)

        results = dict_fetch_all(cursor)
        data_list = []

        if results:
            for data in results:
                data_list.append(data)

        form_caption = "Profissionais Por Categoria"
        return render(request, 'pesquisa/profissionais_por_categoria.html', {'form_caption': form_caption,
                                                                             'data_list': data_list})
    else:
        # Lista de categorias
        form_caption = "Pesquisar Por Categoria"
        data_list = Categoria.objects.all().order_by('nome')
        return render(request, 'pesquisa/lista_categoria.html', {'form_caption': form_caption,
                                                                 'data_list': data_list})


def nome_servico(request):
    form_caption = "Pesquisar Por Nome/Serviço"
    data_list = []

    search_field = ''
    search_value = ''

    if request.GET:
        if ('q' in request.GET) and (request.GET['q'] != ""):
            # search_field = request.GET['searchField']
            search_value = request.GET['q']

            sql_script = "SELECT"
            sql_script += " u.first_name,"
            sql_script += " u.last_name,"
            sql_script += " u.email,"
            sql_script += "COALESCE(CAST((SELECT AVG((nota_atendimento+nota_pontualidade+nota_preco+nota_qualidade+nota_recomendacao)/5) " \
                          "from avaliacao AS a " \
                          "WHERE a.profissional_id=u.id) as integer),0) AS avaliacao, "
            sql_script += " up.fone,"
            sql_script += " up.servico,"
            sql_script += " c.nome AS cat_nome "
            sql_script += "FROM auth_user AS u "
            sql_script += "LEFT JOIN auth_user_profile AS up ON u.id = up.user_id "
            sql_script += "LEFT JOIN categoria AS c ON up.categoria_id = c.id "
            sql_script += "WHERE u.is_active=True "
            sql_script += "AND (lower(u.first_name) LIKE lower('%" + search_value.strip() + "%') "
            sql_script += "OR lower(u.last_name) LIKE lower('%" + search_value.strip() + "%') "
            sql_script += "OR lower(up.servico) LIKE lower('%" + search_value.strip() + "%')) "
            sql_script += "ORDER BY avaliacao DESC, u.first_name, u.last_name"

            # print "--> sql: " + sql_script

            cursor = connection.cursor()
            cursor.execute(sql_script)

            results = dict_fetch_all(cursor)

            if results:
                for data in results:
                    data_list.append(data)

    return render(request, 'pesquisa/nome_servico.html', {'form_caption': form_caption,
                                                          'data_list': data_list,
                                                          'search_value': search_value,
                                                          'search_field': search_field})

"""
||||||||||||||||||||||||
|                      |
|    -= REST API =-    |
|                      |
||||||||||||||||||||||||
"""


@api_view(['POST'])
def api_prof_categ(request):

    if request.POST:

        sql_script = "SELECT"
        sql_script += " u.id,"
        sql_script += " u.first_name,"
        sql_script += " u.last_name,"
        sql_script += " u.email,"
        sql_script += "COALESCE(CAST((SELECT AVG((nota_atendimento+nota_pontualidade+nota_preco+nota_qualidade+nota_recomendacao)/5) " \
                      "from avaliacao AS a " \
                      "WHERE a.profissional_id=u.id) as integer),0) AS avaliacao, "
        sql_script += " up.fone,"
        sql_script += " up.servico,"
        sql_script += " c.nome AS cat_nome "
        sql_script += "FROM auth_user AS u "
        sql_script += "LEFT JOIN auth_user_profile AS up ON u.id = up.user_id "
        sql_script += "LEFT JOIN categoria AS c ON up.categoria_id = c.id "
        sql_script += "WHERE up.categoria_id = " + str(request.POST['categoria_id']) + " "
        sql_script += "AND u.is_active=True "
        sql_script += "AND up.tipo = 'PRO' "
        sql_script += "ORDER BY avaliacao DESC, u.first_name, u.last_name"

        # print "--> sql: " + sql_script

        cursor = connection.cursor()
        cursor.execute(sql_script)

        results = dict_fetch_all(cursor)
        data_list = []

        if results:
            for row in results:
                if row['avaliacao'] == 0:
                    row['stars'] = ""
                elif row['avaliacao'] == 1:
                    row['stars'] = "★"
                elif row['avaliacao'] == 2:
                    row['stars'] = "★ ★"
                elif row['avaliacao'] == 3:
                    row['stars'] = "★ ★ ★"
                elif row['avaliacao'] == 4:
                    row['stars'] = "★ ★ ★ ★"
                elif row['avaliacao'] == 5:
                    row['stars'] = "★ ★ ★ ★ ★"
                data_list.append(row)

        retorno = json.dumps(data_list)

        return HttpResponse(retorno, "application/json")


@api_view(['POST'])  # pesquisa/api/nome
def api_nome_servico(request):
    if request.POST:
        try:
            search_value = request.POST['val_pesq']

            sql_script = "SELECT"
            sql_script += " u.first_name,"
            sql_script += " u.last_name,"
            sql_script += " u.email,"
            sql_script += "COALESCE(CAST((SELECT AVG((nota_atendimento+nota_pontualidade+nota_preco+nota_qualidade+nota_recomendacao)/5) " \
                                         "from avaliacao AS a " \
                                         "WHERE a.profissional_id=u.id) as integer),0) AS avaliacao, "
            sql_script += " up.fone,"
            sql_script += " up.servico,"
            sql_script += " c.nome AS cat_nome "
            sql_script += "FROM auth_user AS u "
            sql_script += "LEFT JOIN auth_user_profile AS up ON u.id = up.user_id "
            sql_script += "LEFT JOIN categoria AS c ON up.categoria_id = c.id "
            sql_script += "WHERE u.is_active=True "
            sql_script += "AND (lower(u.first_name) LIKE lower('%" + search_value.strip() + "%') "
            sql_script += "OR lower(u.last_name) LIKE lower('%" + search_value.strip() + "%') "
            sql_script += "or lower(up.servico) LIKE lower('%" + search_value.strip() + "%')) "
            sql_script += "ORDER BY avaliacao DESC, u.first_name, u.last_name"

            # print "--> sql: " + sql_script

            cursor = connection.cursor()
            cursor.execute(sql_script)

            results = dict_fetch_all(cursor)
            data_list = []

            if results:
                for row in results:

                    if row['avaliacao'] == 0:
                        row['stars'] = ""
                    elif row['avaliacao'] == 1:
                        row['stars'] = "★"
                    elif row['avaliacao'] == 2:
                        row['stars'] = "★ ★"
                    elif row['avaliacao'] == 3:
                        row['stars'] = "★ ★ ★"
                    elif row['avaliacao'] == 4:
                        row['stars'] = "★ ★ ★ ★"
                    elif row['avaliacao'] == 5:
                        row['stars'] = "★ ★ ★ ★ ★"

                    data_list.append(row)

            retorno = json.dumps(data_list)

        except Exception as e:
            ret_json = [{
                'mensagem': e.message,
                'status': 'ERRO'
            }]
            retorno = json.dumps(ret_json)

    return HttpResponse(retorno, "application/json")
