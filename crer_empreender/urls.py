# -*- coding: utf-8 -*-
from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    # Admin
    url(r'^admin/', include(admin.site.urls)),

    # Avaliação
    url(r'^avaliacao/$', 'avaliacao.views.novo', name='avaliacao_novo'),
    url(r'^avaliacao/api/avaliacao/$', 'avaliacao.views.api_avaliacao', name='avaliacao_api_avaliacao'),
    # REST API
    url(r'^categoria/api/lista/$', 'categoria.views.api_lista', name='categoria_api_lista'),

    # Login
    url(r'^login/$', 'login.views.index', name='login_index'),
    url(r'^login/exit/$', 'login.views.exit', name='login_exit'),

    # Menu (DEFAULT PAGE)
    url(r'^$', 'menu.views.index', name='menu_index'),

    # Pesquisa
    url(r'^pesquisa/index/$', 'pesquisa.views.index', name='pesquisa_index'),
    url(r'^pesquisa/categoria/(?P<id>\d+)/$', 'pesquisa.views.categoria', name='pesquisa_categoria'),
    url(r'^pesquisa/nome_servico/$', 'pesquisa.views.nome_servico', name='pesquisa_nome_servico'),
    # REST API
    url(r'^pesquisa/api/prof_categ/$', 'pesquisa.views.api_prof_categ', name='categoria_api_prof_categ'),
    url(r'^pesquisa/api/nome/$', 'pesquisa.views.api_nome_servico', name='pesquisa_api_nome_servico'),

    # Usuário
    url(r'^usuario/$', 'usuario.views.novo', name='usuario_novo'),
    url(r'^usuario/(?P<id>\d+)/$', 'usuario.views.editar', name='usuario_editar'),
    # REST API
    url(r'^usuario/api/login/$', 'usuario.views.api_login', name='usuario_api_login'),
    url(r'^usuario/api/lista_profissional/$', 'usuario.views.api_lista_profissional', name='usuario_api_lista_profissional'),
    url(r'^usuario/api/criar_conta/$', 'usuario.views.api_criar_conta', name='usuario_api_criar_conta'),
    url(r'^usuario/api/consultar_perfil/$', 'usuario.views.api_consultar_perfil', name='usuario_api_consultar_perfil'),
    url(r'^usuario/api/editar_perfil/$', 'usuario.views.api_editar_perfil', name='usuario_api_editar_perfil'),

]
