# -*- coding: utf-8 -*-
from django.contrib.auth import login, logout
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render

from login.forms import LoginForm
from django.forms.util import ErrorList


def index(request):
    # Exemplo de captura de variável de configuração
    # BASE_DIR = getattr(settings, "BASE_DIR", None)

    if request.POST:
        form = LoginForm(request.POST)
        if form.is_valid():
            user = form.save()

            print user.is_active

            if not user.is_active:
                errors = form._errors.setdefault("username", ErrorList())
                errors.append(u"Não ativado.")
            else:
                login(request, user)
                return HttpResponseRedirect(reverse('menu_index'))
    else:
        form = LoginForm()

    return render(request, 'login/index.html', {'form': form})


def exit(request):

    logout(request)
    return HttpResponseRedirect(reverse('login_index'))
