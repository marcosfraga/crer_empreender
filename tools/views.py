# -*- coding: utf-8 -*-


def dict_fetch_all(cursor):
    # Return all rows from a cursor as a dict
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]


def float_format(value, places=2):
    if places == 2:
        value = '{:15.2f}'.format(value).lstrip()
    elif places == 3:
        value = '{:15.3f}'.format(value).lstrip()

    value = value.replace('.', ',')

    return value


def ajusta_acento(texto):
    retorno = texto

    retorno = retorno.replace('\\xc1', 'Á')

    retorno = retorno.replace('\\xc2', 'Â')

    retorno = retorno.replace('\\xc3', 'Ã')

    retorno = retorno.replace('\\xc7', 'Ç')

    retorno = retorno.replace('\\xc9', 'É')

    retorno = retorno.replace("\\xe1", "á")

    retorno = retorno.replace('\\xe2', 'â')

    retorno = retorno.replace('\\xe3', 'ã')

    retorno = retorno.replace("\\xe4", "ä")

    retorno = retorno.replace('\\xe7', 'ç')

    retorno = retorno.replace('\\xe9', 'é')

    retorno = retorno.replace('\\xed', 'í')

    retorno = retorno.replace('\\xf3', 'ó')

    retorno = retorno.replace('\\xfa', 'ú')

    retorno = retorno.replace('\\xcd', 'Í')

    retorno = retorno.replace('\\xd3', 'Ó')

    retorno = retorno.replace('\\xda', 'Ú')

    retorno = retorno.replace('\\xf5', 'õ')

    retorno = retorno.replace('\\xd5', 'Õ')

    retorno = retorno.replace('\\xf4', 'ô')

    retorno = retorno.replace('\\xea', 'ê')

    retorno = retorno.replace('\\xd4', 'Ô')

    retorno = retorno.replace('\\xdb', 'Û')

    retorno = retorno.replace('\\xeb', 'ë')

    retorno = retorno.replace('\\xef', 'ï')

    retorno = retorno.replace('\\xf6', 'ö')

    retorno = retorno.replace('\\xfc', 'ü')

    retorno = retorno.replace('\\xc4', 'Ä')

    retorno = retorno.replace('\\xcb', 'Ë')

    retorno = retorno.replace('\\xcf', 'Ï')

    retorno = retorno.replace('\\xd6', 'Ö')

    retorno = retorno.replace('\\xdc', 'Ü')

    retorno = retorno.replace('\\u2022', '•')

    return retorno
