# -*- coding: utf-8 -*-
from rest_framework import serializers

from categoria.models import Categoria


class CategoriaSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=False)

    class Meta:
        model = Categoria
        fields = (
            'id', 'nome', 'icone'
        )
