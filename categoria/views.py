# -*- coding: utf-8 -*-
import json

from django.http import HttpResponse
from rest_framework.decorators import api_view

from categoria.models import Categoria


"""
||||||||||||||||||||||||
|                      |
|    -= REST API =-    |
|                      |
||||||||||||||||||||||||
"""


@api_view(['GET'])
def api_lista(request):  # /categoria/api/lista/?format=json
    if request.method == 'GET':
        obj_list = Categoria.objects.filter().order_by('nome')

        data_list = []

        for row in obj_list:
            data_row = {
                'id': row.id,
                'nome': row.nome,
                'descricao': row.descricao,
                'icone': row.icone,
                'cor_icone': row.cor_icone,
            }
            data_list.append(data_row)

        retorno = json.dumps(data_list)

        return HttpResponse(retorno, "application/json")