# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('categoria', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='categoria',
            name='cor_icone',
            field=models.CharField(max_length=10, null=True, verbose_name='Cor \xcdcone', blank=True),
        ),
        migrations.AddField(
            model_name='categoria',
            name='descricao',
            field=models.CharField(max_length=60, null=True, verbose_name='Descri\xe7\xe3o', blank=True),
        ),
        migrations.AlterField(
            model_name='categoria',
            name='icone',
            field=models.CharField(max_length=30, null=True, verbose_name='\xcdcone', blank=True),
        ),
    ]
