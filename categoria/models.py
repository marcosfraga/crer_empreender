# -*- coding: utf-8 -*-
from django.db import models


class Categoria(models.Model):
    nome = models.CharField(
        u'Nome',
        max_length=60
    )
    descricao = models.CharField(
        u'Descrição',
        max_length=60,
        blank=True,
        null=True,
    )
    icone = models.CharField(
        u'Ícone',
        max_length=30,
        blank=True,
        null=True,
    )
    cor_icone = models.CharField(
        u'Cor Ícone',
        max_length=10,
        blank=True,
        null=True,
    )

    class Meta:
        db_table = 'categoria'

    def __unicode__(self):
        return self.nome
