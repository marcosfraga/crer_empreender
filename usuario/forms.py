# -*- coding: utf-8 -*-
from django import forms

from categoria.models import Categoria
from usuario.models import UserProfile


class UsuarioForm(forms.Form):
    tipo = forms.ChoiceField(
        choices=UserProfile.TIPO_CHOICES,
        label='Tipo',
        widget=forms.Select(attrs={'class': 'select'})
    )
    primeiro_nome = forms.CharField(
        label='Primeiro Nome',
        max_length=60,
    )
    ultimo_nome = forms.CharField(
        label='Último Nome',
        max_length=60,
    )
    email = forms.CharField(
        label='Email',
        max_length=60,
    )
    nome_usuario = forms.CharField(
        label='Usuário',
        max_length=30,

    )
    senha = forms.CharField(
        label='Senha',
        max_length=20,
        required=False,
    )
    conf_senha = forms.CharField(
        label='Confirmação de Senha',
        max_length=20,
        required=False,
    )
    fone = forms.CharField(
        label='Fone',
        max_length=20,
    )
    categoria = forms.ModelChoiceField(
        queryset=Categoria.objects.all().order_by('nome'),
        label='Categoria',
        widget=forms.Select(attrs={'class': 'select'}),
        required = False,
    )
    servico = forms.CharField(
        label='Serviço/Produto',
        max_length=254,
        required=False,
    )
