# -*- coding: utf-8 -*-
from rest_framework import serializers

from django.contrib.auth.models import User
from usuario.models import UserProfile


class UserSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=False)

    class Meta:
        model = User
        fields = (
            'id', 'username', 'first_name', 'last_name', 'email', 'password'
        )


class UserProfileSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=False)

    class Meta:
        model = UserProfile
        fields = (
            'id', 'tipo', 'fone', 'categoria', 'servico'
        )
