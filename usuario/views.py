# -*- coding: utf-8 -*-
import json

from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.forms.forms import NON_FIELD_ERRORS
from django.forms.util import ErrorList
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.shortcuts import render
from rest_framework.decorators import api_view

from usuario.forms import UsuarioForm
from usuario.models import UserProfile


# -= NOVO =-
def novo(request):
    form_action = "/usuario/"

    if request.POST:
        form = UsuarioForm(request.POST)
        if form.is_valid():
            form_valido = True

            if form.cleaned_data['senha'] != form.cleaned_data['conf_senha']:
                errors = form._errors.setdefault("senha", ErrorList())
                errors.append(u"As senhas não conferem.")
                form_valido = False

            if form.cleaned_data['tipo'] == "PRO" and form.cleaned_data['categoria'] is None:
                errors = form._errors.setdefault("categoria", ErrorList())
                errors.append(u"Deve ser informada.")
                form_valido = False

            if form.cleaned_data['tipo'] == "PRO" and form.cleaned_data['servico'] == "":
                errors = form._errors.setdefault("servico", ErrorList())
                errors.append(u"Deve ser informado.")
                form_valido = False

            if form_valido:
                # Verificar se nome usuário existe
                user = User.objects.filter(username=form.cleaned_data['nome_usuario'])

                if not user:
                    # Criar Novo Usuário
                    new_user = User()
                    new_user.username = form.cleaned_data['nome_usuario']
                    new_user.first_name = form.cleaned_data['primeiro_nome']
                    new_user.last_name = form.cleaned_data['ultimo_nome']
                    new_user.email = form.cleaned_data['email']
                    new_user.set_password(form.cleaned_data['senha'])
                    new_user.is_active = False
                    new_user.save()
                    # Atualizar Profile
                    user_profile = UserProfile.objects.get(user_id=new_user.id)
                    user_profile.tipo = form.cleaned_data['tipo']
                    user_profile.fone = form.cleaned_data['fone']
                    user_profile.categoria = form.cleaned_data['categoria']
                    user_profile.servico = form.cleaned_data['servico']
                    user_profile.save()

                    form.errors[NON_FIELD_ERRORS] = form.error_class(["Conta criada com sucesso."])
                    return render(request, 'usuario/conta_criada.html', {})
                else:
                    errors = form._errors.setdefault("nome_usuario", ErrorList())
                    errors.append(u"Nome de usuário já está em uso.")

    else:
        form = UsuarioForm()
        form.fields['tipo'].initial = ""
        form.fields['primeiro_nome'].initial = ""
        form.fields['ultimo_nome'].initial = ""
        form.fields['email'].initial = ""
        form.fields['nome_usuario'].initial = ""
        form.fields['senha'].initial = ""
        form.fields['conf_senha'].initial = ""
        form.fields['fone'].initial = ""
        form.fields['categoria'].initial = ""
        form.fields['servico'].initial = ""

    form_caption = "Criar Conta"
    return render(request, 'usuario/form.html', {'form': form,
                                                 'form_action': form_action,
                                                 'form_caption': form_caption})


# -= EDITAR =-
@login_required
def editar(request, id):
    form_action = "/usuario/"+id+"/"
    form_caption = "Alterar Perfil"

    user = get_object_or_404(User, id=id)

    if request.POST:
        form = UsuarioForm(request.POST)

        if form.is_valid():
            # Atualizar Usuário
            user.username = form.cleaned_data['nome_usuario']
            user.first_name = form.cleaned_data['primeiro_nome']
            user.last_name = form.cleaned_data['ultimo_nome']
            user.email = form.cleaned_data['email']
            user.save()
            # Atualizar Profile
            user_profile = UserProfile.objects.get(user_id=user.id)
            user_profile.tipo = form.cleaned_data['tipo']
            user_profile.fone = form.cleaned_data['fone']
            user_profile.categoria = form.cleaned_data['categoria']
            user_profile.servico = form.cleaned_data['servico'].strip(' \t\n\r')
            user_profile.save()

            return render(request, 'usuario/perfil_alterado.html', {})
        else:
            return render(request, 'usuario/form.html', {'form_caption': form_caption,
                                                         'form_action': form_action,
                                                         'form': form})
    else:
        user_profile = UserProfile.objects.get(user_id=user.id)

        form = UsuarioForm()
        form.fields['tipo'].initial = user_profile.tipo
        form.fields['primeiro_nome'].initial = user.first_name
        form.fields['ultimo_nome'].initial = user.last_name
        form.fields['email'].initial = user.email
        form.fields['nome_usuario'].initial = user.username
        form.fields['senha'].initial = ""
        form.fields['conf_senha'].initial = ""
        form.fields['fone'].initial = user_profile.fone
        form.fields['categoria'].initial = user_profile.categoria
        form.fields['servico'].initial = user_profile.servico.strip(' \t\n\r')

    return render(request, 'usuario/form.html', {'form_caption': form_caption,
                                                 'form_action': form_action,
                                                 'form': form})


"""
||||||||||||||||||||||||
|                      |
|    -= REST API =-    |
|                      |
||||||||||||||||||||||||
"""


@api_view(['POST'])
def api_login(request):

    if request.POST:

        user = authenticate(username=request.POST['usuario'], password=request.POST['senha'])

        if user:
            if user.is_active:
                ret_json = {
                    'id': user.id,
                    'first_name': user.first_name,
                    'last_name': user.last_name,
                    'superuser': user.is_superuser,
                    'mensagem': 'Login Efetuado.',
                    'status': 'OK'
                }
            else:
                ret_json = {
                    'mensagem': 'Usuário não foi ativado.',
                    'status': 'INATIVO'
                }
        else:
            ret_json = {
                'nome': None,
                'mensagem': "Usuário ou senha não confere.",
                'status': "NAO_ENCONTRADO"
            }
    else:
        ret_json = {
            'mensagem': 'Problema ao efetuar login.',
            'status': 'ERRO'
        }

    return HttpResponse(json.dumps(ret_json), "application/json")


@api_view(['POST'])
def api_criar_conta(request):

    if request.POST:

        try:
            # Verificar se nome usuário existe
            user = User.objects.filter(username=request.POST['n_usuario'])

            if not user:
                # Criar Novo Usuário
                new_user = User()
                new_user.username = request.POST['n_usuario']
                new_user.first_name = request.POST['p_nome']
                new_user.last_name = request.POST['u_nome']
                new_user.email = request.POST['email']
                new_user.set_password(request.POST['senha'])
                new_user.is_active = False
                new_user.save()
                # Atualizar Profile
                user_profile = UserProfile.objects.get(user_id=new_user.id)
                user_profile.tipo = request.POST['tipo']
                user_profile.fone = request.POST['fone']
                if request.POST['categoria']:
                    user_profile.categoria_id = request.POST['categoria']
                if request.POST['serv_prod']:
                    user_profile.servico = request.POST['serv_prod']

                user_profile.save()

                ret_json = {'mensagem': 'Conta Criada com Sucesso!'}
            else:
                ret_json = {'mensagem': 'JA_EXISTE'}

        except Exception as e:
            ret_json = {'mensagem': 'Erro->' + e.message}

        return HttpResponse(json.dumps(ret_json), "application/json")


@api_view(['POST'])
def api_consultar_perfil(request):
    if request.POST:

        user = User.objects.get(id=request.POST['user_id'])
        user_profile = UserProfile.objects.get(user_id=user.id)
        if user:
            ret_json = {
                'id': user.id,
                'tipo': user_profile.tipo,
                'nome': user.first_name,
                'sobrenome': user.last_name,
                'email': user.email,
                'usuario': user.username,
                'fone': user_profile.fone,
                'categoria': user_profile.categoria_id,
                'servprod': user_profile.servico,
            }
    else:
        ret_json = {'mensagem': 'ERRO'}

    return HttpResponse(json.dumps(ret_json), "application/json")


@api_view(['POST'])
def api_editar_perfil(request):

    if request.POST:

        try:
            user = User.objects.get(id=request.POST['id'])
            if user:
                user.first_name = request.POST['nome']
                user.last_name = request.POST['sobrenome']
                # user.username = request.POST['usuario']
                user.email = request.POST['email']
                user.save()
                # Atualizar Profile
                user_profile = UserProfile.objects.get(user_id=user.id)
                user_profile.tipo = request.POST['tipo']
                user_profile.fone = request.POST['fone']
                user_profile.categoria_id = request.POST['categoria']
                user_profile.servico = request.POST['servprod']
                user_profile.save()

                ret_json = {
                    'mensagem': 'Perfil alterado com sucesso!',
                    'status': 'OK'
                }
            else:
                ret_json = {
                    'mensagem': 'Perfil não encontratdo!',
                    'status': 'NAO_ENCONTRADO'
                }
        except Exception as e:
            print "---ERRO: " + e.message
            ret_json = {
                'mensagem': e.message,
                'status': 'ERRO'
            }

        print str(ret_json)

        return HttpResponse(json.dumps(ret_json), "application/json")


@api_view(['GET'])
def api_lista_profissional(request):  # /usuario/api/lista_profissional/
    if request.method == 'GET':
        obj_list = User.objects.filter(is_active=True).order_by('first_name', 'last_name')

        data_list = []

        for row in obj_list:
            data_row = {'id': row.id, 'nome': row.first_name + ' ' + row.last_name}
            data_list.append(data_row)

        retorno = json.dumps(data_list)

        return HttpResponse(retorno, "application/json")
