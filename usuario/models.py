# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User


class UserProfile(models.Model):
    TIPO_CHOICES = (
        ('CLI', 'Cliente'),
        ('PRO', 'Profissional'),
    )

    tipo = models.CharField(
        u'Tipo(Sigla)',
        max_length=3,
        choices=TIPO_CHOICES,
    )
    user = models.OneToOneField(
        User,
        related_name='user_profile'
    )
    fone = models.CharField(
        u'Fone',
        max_length=15,
        blank=True,
        null=True,
    )
    servico = models.CharField(
        u'Serviço',
        max_length=256,
        blank=True,
    )
    foto = models.ImageField(
        u'Foto',
        blank=True,
    )
    categoria = models.ForeignKey(
        'categoria.Categoria',
        null=True,
    )

    class Meta:
        db_table = 'auth_user_profile'

    def __unicode__(self):
        return self.user.username
