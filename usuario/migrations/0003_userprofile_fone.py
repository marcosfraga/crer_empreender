# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('usuario', '0002_userprofile_categoria'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='fone',
            field=models.CharField(max_length=15, null=True, verbose_name='Fone', blank=True),
        ),
    ]
