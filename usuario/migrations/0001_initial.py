# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tipo', models.CharField(max_length=3, verbose_name='Tipo(Sigla)', choices=[(b'CLI', b'Cliente'), (b'PRO', b'Profissional')])),
                ('servico', models.CharField(max_length=256, verbose_name='Servi\xe7o', blank=True)),
                ('foto', models.ImageField(upload_to=b'', verbose_name='Foto', blank=True)),
                ('user', models.OneToOneField(related_name='user_profile', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'auth_user_profile',
            },
        ),
    ]
