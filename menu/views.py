# -*- coding: utf-8 -*-
from django.contrib.auth.decorators import login_required
from django.shortcuts import render


@login_required
def index(request):
    form_caption = "Menu"
    form_sub_caption = ""

    # if request.POST:
        # DO THINGS

    return render(request, 'menu/index.html', {'form_caption': form_caption,
                                               'form_sub_caption': form_sub_caption})

